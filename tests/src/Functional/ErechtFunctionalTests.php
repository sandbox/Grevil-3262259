<?php

namespace Drupal\Tests\erecht_legal_texts\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group erecht_legal_texts
 */
class ErechtFunctionalTests extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'text',
    'filter',
    'token_filter',
    'block',
    'token',
    'erecht_legal_texts',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests the 'administer erecht_legal_texts configuration' permission.
   */
  public function testPermissionAdministerConfig() {
    $session = $this->assertSession();
    // Go to settings page as an admin:
    $this->drupalGet('/admin/config/system/erecht-legal-texts');
    $session->statusCodeEquals(200);
    // Go to settings page as an authenticated user with the right permission:
    $this->drupalLogout();
    $userWithPerm = $this->drupalCreateUser(['administer erecht_legal_texts configuration']);
    $this->drupalLogin($userWithPerm);
    $this->drupalGet('/admin/config/system/erecht-legal-texts');
    $session->statusCodeEquals(200);
    // Go to settings page as an authenticated user without permission:
    $this->drupalLogout();
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/config/system/erecht-legal-texts');
    $session->statusCodeEquals(403);
    // Go to settings page as an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('/admin/config/system/erecht-legal-texts');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests the ObserverConrtollerAccess.
   */
  public function testObserverNotification() {
    $session = $this->assertSession();
    $secret = $this->config('erecht_legal_texts.settings')->get('notification_callback_secret');
    $this->drupalGet('/erecht-legal-texts/notification-callback/this-should-be-forbidden');
    $session->statusCodeEquals(403);
    $this->drupalGet('/erecht-legal-texts/notification-callback/' . $secret);
    $session->statusCodeEquals(200);
  }

}
