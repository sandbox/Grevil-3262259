<?php

namespace Drupal\erecht_legal_texts\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\erecht_legal_texts\ErechtTextObserver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Erecht-Legal-Texts routes.
 */
class ObserverNotificationController extends ControllerBase {

  /**
   * The legalTextManager object.
   *
   * @var Drupal\erecht_legal_texts\ErechtTextObserver
   */
  protected $eRechtTextObserver;

  /**
   * {@inheritDoc}
   */
  public function __construct(ErechtTextObserver $eRechtTextObserver) {
    $this->eRechtTextObserver = $eRechtTextObserver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('erecht_legal_texts.text_observer')
    );
  }

  /**
   * This is called from eRecht on legal text changes.
   *
   * This is called from eRecht on legal text changes,
   * if we registered as notification target before.
   */
  public function notify($secret) {
    try {
      $this->eRechtTextObserver->refreshLegalTexts();
    }
    catch (\Exception $e) {
      // @todo Replace in Drupal 10, see: https://www.drupal.org/project/drupal/issues/2932518
      watchdog_exception('erecht_legal_texts', $e);
      return [
        '#markup' => 'Error',
      ];
    }
    return [
      '#markup' => 'Success',
    ];
  }

  /**
   * Checks access for this controller.
   */
  public function access($secret) {
    $config = $this->config('erecht_legal_texts.settings');
    if ($secret == $config->get('notification_callback_secret')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
