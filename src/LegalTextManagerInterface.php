<?php

namespace Drupal\erecht_legal_texts;

/**
 * Provides exposed LegalTextManager functionalities.
 */
interface LegalTextManagerInterface {

  /**
   * Get the string / format array from the legal text entity 'imprint'.
   */
  public function getImprint(string $lang_code): array;

  /**
   * Get the string / format array from the legal text entity 'privacy_policy'.
   */
  public function getPrivacyPolicy(string $lang_code): array;

  /**
   * Get the string/format array from the entity 'privacy_policy_social_media'.
   */
  public function getPrivacyPolicySocialMedia(string $lang_code): array;

}
