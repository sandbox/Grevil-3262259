<?php

namespace Drupal\erecht_legal_texts;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\erecht_legal_texts\Helper\Constants;

/**
 * Provides Erecht legal texts functionalities.
 */
class LegalTextManager implements LegalTextManagerInterface {

  /**
   * An ErechtTextFetcher object.
   *
   * @var Drupal\erecht_legal_texts\ErechtTextFetcher
   */
  protected $eRechtTextFetcher;

  /**
   * An entityTypeManager object.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The Constructor.
   */
  public function __construct(ErechtTextFetcher $eRechtTextFetcher, EntityTypeManagerInterface $entityTypeManager, AccountProxy $currentUser) {
    $this->eRechtTextFetcher = $eRechtTextFetcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritDoc}
   */
  public function getImprint(string $lang_code = 'de'): array {
    $entity = $this->getLegalTextEntityByType(Constants::TYPE_IMPRINT, $lang_code);
    // If disabled return empty text:
    if (empty($entity->get('enabled')->value)) {
      return ['value' => '', 'format' => filter_default_format()];
    }
    return $entity->get('legal_text')->getValue()[0];
  }

  /**
   * {@inheritDoc}
   */
  public function getPrivacyPolicy(string $lang_code = 'de'): array {
    $entity = $this->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY, $lang_code);
    // If disabled return empty text:
    if (empty($entity->get('enabled')->value)) {
      return ['value' => '', 'format' => filter_default_format()];
    }
    return $entity->get('legal_text')->getValue()[0];
  }

  /**
   * {@inheritDoc}
   */
  public function getPrivacyPolicySocialMedia(string $lang_code = 'de'): array {
    $entity = $this->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA, $lang_code);
    // If disabled return empty text:
    if (empty($entity->get('enabled')->value)) {
      return ['value' => '', 'format' => filter_default_format()];
    }
    return $entity->get('legal_text')->getValue()[0];
  }

  /**
   * Refreshes the legal text for the given entity.
   *
   * @param string $type
   *   The legal text type to refresh.
   * @param string $lang_code
   *   The legal text type language.
   */
  public function refreshLegalText(string $type, string $lang_code = 'de') {
    try {
      $entity = $this->getLegalTextEntityByType($type);
      $legalText = $this->eRechtTextFetcher->fetchTextByType($type, $lang_code);
    }
    catch (\Throwable $th) {
      throw $th;
    }
    if (empty($entity->get('custom')->value) || empty($entity->get('enabled')->value)) {
      $entity->set('legal_text', $legalText)->save();
    }
  }

  /**
   * Refreshes all legal texts in the supported languages.
   */
  public function refreshAllLegalTexts() {
    $languages = Constants::TEXTS_LANGUAGES;
    $textsTypes = Constants::TEXTS_TYPES;
    // Loop all text types and languages:
    foreach ($textsTypes as $textType) {
      foreach ($languages as $language) {
        try {
          $this->refreshLegalText($textType, $language);
        }
        catch (\Throwable $th) {
          throw $th;
        }
      }
    }
  }

  /**
   * Get the legal text by legal text entity type.
   *
   * @param string $type
   *   The legal text type.
   * @param string $language_code
   *   The legal text language.
   */
  public function getLegalTextEntityByType($type, $language_code = 'de') {
    // @todo Return translated entity object.
    // If yes, set values and create a new revision
    $erechtLegalTextEntityStorage = $this->entityTypeManager->getStorage('erecht_legal_text');
    // @todo Filter by language:
    $entities = $erechtLegalTextEntityStorage->loadByProperties(['machine_name' => $type]);
    if (!empty($entities)) {
      if (count($entities) > 1) {
        throw new \Exception('More than one entity! This should not happen!');
      }
      $entity = reset($entities);
      if (empty($entity->get('legal_text')->getString())) {
        $legalText = $this->eRechtTextFetcher->fetchTextByType($type, $language_code);
        $entity->set('legal_text', [
          'value' => $legalText,
          'format' => filter_default_format(),
        ])->save();
      }
    }
    else {
      $legal_text = $this->eRechtTextFetcher->fetchTextByType($type, $language_code);
      $entity = $erechtLegalTextEntityStorage->create([
        'machine_name' => $type,
        'uid' => $this->currentUser->id(),
        'legal_text' => [
          'value' => $legal_text ?? '',
          'format' => filter_default_format(),
        ],
        'custom' => FALSE,
        'auto_update' => TRUE,
        'enabled' => FALSE,
      ])->save();
    }
    return $entity;
  }

  /**
   * Creates empty legal text entities.
   */
  public function createEmptyEntities() {
    $erechtLegalTextEntityStorage = $this->entityTypeManager->getStorage('erecht_legal_text');
    $entities = $erechtLegalTextEntityStorage->loadByProperties(['machine_name' => Constants::TEXTS_TYPES]);
    if (empty($entities)) {
      foreach (Constants::TEXTS_TYPES as $type) {
        $erechtLegalTextEntityStorage->create([
          'machine_name' => $type,
          'uid' => $this->currentUser->id(),
          'legal_text' => [
            'value' => '',
            'format' => filter_default_format(),
          ],
          'custom' => FALSE,
          'auto_update' => TRUE,
          'enabled' => FALSE,
        ])->save();
      }
    }
    else {
      throw new \Exception('The entities already exist! This should not happen.');
    }
  }

}
