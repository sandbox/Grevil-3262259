<?php

namespace Drupal\erecht_legal_texts\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\erecht_legal_texts\Helper\Constants;
use Drupal\erecht_legal_texts\LegalTextManagerInterface;
use eRecht24\RechtstexteSDK\Exceptions\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an erecht block.
 *
 * @Block(
 *   id = "erecht_legal_texts",
 *   admin_label = @Translation("eRecht legal text"),
 *   category = @Translation("eRecht legal texts")
 * )
 */
class ErechtBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * A LegalTextManagerInterface object.
   *
   * @var Drupal\erecht_legal_texts\LegalTextManagerInterface
   */
  protected $legalTextManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LegalTextManagerInterface $legalTextManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->legalTextManager = $legalTextManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('erecht_legal_texts.legal_text_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $legalProcessedText = [];
    switch ($this->getConfiguration()['legal_text_type']) {
      case Constants::TYPE_IMPRINT:
        $legalProcessedText = $this->legalTextManager->getImprint();
        break;

      case Constants::TYPE_PRIVACY_POLICY:
        $legalProcessedText = $this->legalTextManager->getPrivacyPolicy();
        break;

      case Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA:
        $legalProcessedText = $this->legalTextManager->getPrivacyPolicySocialMedia();
        break;

      default:
        throw new Exception("This legal text type is unknown!");

    }
    $build['content'] = [
      '#type' => 'processed_text',
      '#text' => $legalProcessedText['value'],
      '#format' => $legalProcessedText['format'],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'legal_text_type' => Constants::TYPE_IMPRINT,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['legal_text_type'] = [
      '#type' => 'radios',
      '#title' => 'Define which legal text you would like to display.',
      '#options' => [
        Constants::TYPE_IMPRINT => $this->t('Imprint'),
        Constants::TYPE_PRIVACY_POLICY => $this->t('Privacy Policy'),
        Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA => $this->t('Privacy Policy Social Media'),
      ],
      '#default_value' => $this->configuration['legal_text_type'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['legal_text_type']
      = $form_state->getValue('legal_text_type');
  }

}
