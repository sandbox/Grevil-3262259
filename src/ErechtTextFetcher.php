<?php

namespace Drupal\erecht_legal_texts;

use Drupal\Core\Config\ConfigFactory;
use Drupal\erecht_legal_texts\Helper\Constants;
use eRecht24\RechtstexteSDK\LegalTextHandler;
use eRecht24\RechtstexteSDK\Exceptions\Exception;
use Drupal\erecht_legal_texts\Helper\Validation;

/**
 * Provides eRecht legal texts functionalities.
 */
class ErechtTextFetcher {

  /**
   * A config factory object.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The config object.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Constructor.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
    $this->config = $this->configFactory->get('erecht_legal_texts.settings');
  }

  /**
   * Fetches a specific legal text by type and language.
   *
   * @param string $type
   *   The legal text type.
   * @param string $language_code
   *   The language for the legal text.
   *
   * @return string
   *   The legal text in the required language and type.
   */
  public function fetchTextByType(string $type, string $language_code): string {
    // Convert our type to the remote push type name:
    $pushType = Constants::PUSH_TYPE_MAPPING[$type];
    $apiKey = $this->config->get('api_key');
    if (!Validation::isApiKeyValid($apiKey)) {
      throw new Exception('No or invalid API Key given. Check the module settings page.');
    }
    $pluginKey = $this->config->get('plugin_key');
    if (!Validation::isPluginKeyValid($pluginKey)) {
      throw new Exception('No or invalid Plugin Key given.');
    }
    $legalTextHandler = new LegalTextHandler($apiKey, $pushType, $pluginKey);
    $text = $this->fetchInLanguage($legalTextHandler, $language_code);

    return $text;
  }

  /**
   * Returns the text based on the LegalTextHandler and the language code.
   *
   * @param \eRecht24\RechtstexteSDK\LegalTextHandler $legalTextHandler
   *   The LegalTextHandler object with the legal text type.
   * @param string $language_code
   *   The language to retrieve the legal text in.
   *
   * @return string
   *   The text to return.
   */
  private function fetchInLanguage(LegalTextHandler $legalTextHandler, string $language_code): string {
    $text = '';
    /**
     * @var LegalText $legalText
     */
    $legalTextDoc = $legalTextHandler->importDocument();
    switch ($language_code) {
      // German:
      case 'de':
        $text = $legalTextDoc->getHtmlDE();
        break;

      // English:
      case 'en':
        $text = $legalTextDoc->getHtmlEN();
        break;

      default:
        throw new Exception('Language ' . $language_code . 'is not yet available.');
    }
    if (empty($text)) {
      throw new Exception('Text could not be fetchen in language: ' . $language_code);
    }
    return $text;
  }

}
