<?php

namespace Drupal\erecht_legal_texts\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\erecht_legal_texts\ErechtLegalTextInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the erecht legal text entity class.
 *
 * @ContentEntityType(
 *   id = "erecht_legal_text",
 *   label = @Translation("eRecht Legal Text"),
 *   label_collection = @Translation("eRecht Legal Texts"),
 *   label_singular = @Translation("erecht legal text"),
 *   label_plural = @Translation("erecht legal texts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count erecht legal texts",
 *     plural = "@count erecht legal texts",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\erecht_legal_texts\ErechtLegalTextListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\erecht_legal_texts\Form\ErechtLegalTextForm",
 *       "edit" = "Drupal\erecht_legal_texts\Form\ErechtLegalTextForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\erecht_legal_texts\Routing\ErechtLegalTextHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "erecht_legal_text",
 *   data_table = "erecht_legal_text_field_data",
 *   revision_table = "erecht_legal_text_revision",
 *   revision_data_table = "erecht_legal_text_field_revision",
 *   show_revision_ui = FALSE,
 *   translatable = TRUE,
 *   admin_permission = "administer erecht_legal_texts configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/erecht-legal-text",
 *     "canonical" = "/erecht-legal-text/{erecht_legal_text}",
 *     "edit-form" = "/erecht-legal-text/{erecht_legal_text}",
 *   },
 * )
 */
class ErechtLegalText extends RevisionableContentEntityBase implements ErechtLegalTextInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // @todo Prevent duplicate machine_name. Look for documentation how to make it unique (with language)!
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Legal text type'))
      ->setDescription(t('A unique machine-readable name for this text type. It must only contain lowercase letters, numbers, and underscores.'))
      ->setSetting('max_length', 64);

    // @todo Change to text / form field or create two fields:
    $fields['legal_text'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      // @todo LATER: Change later to translatable
      ->setTranslatable(FALSE)
      ->setLabel(t('Legal text'))
      // @todo Change to wysiwyg editor later:
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the erecht legal text was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the erecht legal text was last edited.'));

    $fields['custom'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Custom'))
      ->setDescription(t('See if this entity was edited.'));

    $fields['auto_update'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Automatic updates'))
      ->setDescription(t('Wheter or not this entity should be automatically updated'));

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('Wheter or not this entity is enabled and therefore is used by the user.'));
    return $fields;
  }

}
