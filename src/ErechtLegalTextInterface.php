<?php

namespace Drupal\erecht_legal_texts;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an erecht legal text entity type.
 */
interface ErechtLegalTextInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
