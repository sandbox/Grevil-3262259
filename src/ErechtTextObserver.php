<?php

namespace Drupal\erecht_legal_texts;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use eRecht24\RechtstexteSDK\ApiHandler;
use eRecht24\RechtstexteSDK\Model\Client;
use eRecht24\RechtstexteSDK\Exceptions\Exception;
use Drupal\Core\Url;

/**
 * Provides Erecht legal texts functionalities.
 *
 * Note this seems quite dirty, because of the client
 * implementation of the RechtstexteSDK.
 */
class ErechtTextObserver {

  /**
   * An eRecht24 API Handler Object.
   *
   * @var eRecht24\RechtstexteSDK\ApiHandler
   */
  protected $apiHandler;

  /**
   * A config factory object.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The config object.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The legalTextManager object.
   *
   * @var Drupal\erecht_legal_texts\LegalTextManagerInterface
   */
  protected $legalTextManager;

  /**
   * The RechtstexteSDK client.
   *
   * @var eRecht24\RechtstexteSDK\Model\Client
   */
  protected $client;

  /**
   * The logger factory instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Constructor.
   */
  public function __construct(ConfigFactory $configFactory, LegalTextManagerInterface $legalTextManager, LoggerChannelFactory $loggerFactory) {
    $this->configFactory = $configFactory;
    $this->legalTextManager = $legalTextManager;
    $this->logger = $loggerFactory->get('erecht_legal_texts');
    $this->config = $this->configFactory->get('erecht_legal_texts.settings');
    $this->client = new Client();
    try {
      $this->apiHandler = new ApiHandler($this->config->get('api_key'), $this->config->get('plugin_key'));
    }
    catch (\Throwable $e) {
      $this->logger->error($e->__toString());
    }
  }

  /**
   * Registers at eRecht24 as notificaiton observer.
   */
  public function register() {
    $websiteOwnerEmail = $this->configFactory->get('system.site')->get('mail');
    $notification_callback_secret = $this->config->get('notification_callback_secret');
    $this->client
      ->setPushMethod('POST')
      ->setPushUri(Url::fromRoute('erecht_legal_texts.notification_callback', ['secret' => $notification_callback_secret], ['absolute' => TRUE])->toString())
      ->setCms('Drupal')
      ->setCmsVersion(\Drupal::VERSION)
      ->setPluginName('drupal/erecht_legal_texts')
      ->setAuthorMail($websiteOwnerEmail);
    try {
      // Register as notification client:
      $this->client = $this->apiHandler->createClient($this->client);
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Updates the registration data of the client.
   */
  public function updateRegistration() {
    $websiteOwnerEmail = $this->configFactory->get('system.site')->get('mail');
    $notification_callback_secret = $this->config->get('notification_callback_secret');
    $this->client
      ->setPushMethod('POST')
      ->setPushUri(Url::fromRoute('erecht_legal_texts.observer_notification', ['secret' => $notification_callback_secret], ['absolute' => TRUE])->toString())
      ->setCms('Drupal')
      ->setCmsVersion(\Drupal::VERSION)
      ->setPluginName('drupal/erecht_legal_texts')
      ->setAuthorMail($websiteOwnerEmail);
    $this->apiHandler->updateClient($this->client);
  }

  /**
   * Delegates legal text refresh to legal text manager.
   */
  public function refreshLegalTexts() {
    $this->legalTextManager->refreshAllLegalTexts();
  }

}
