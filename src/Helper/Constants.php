<?php

namespace Drupal\erecht_legal_texts\Helper;

use eRecht24\RechtstexteSDK\Helper\Helper;

/**
 * Constants for erecht_legal_texts.
 */
class Constants {

  /**
   * The erecht_legal_texts imprint type.
   */
  const TYPE_IMPRINT = 'imprint';

  /**
   * The erecht_legal_texts privacy policy type.
   */
  const TYPE_PRIVACY_POLICY = 'privacy_policy';

  /**
   * The erecht_legal_texts privacy policy social media type.
   */
  const TYPE_PRIVACY_POLICY_SOCIAL_MEDIA = 'privacy_policy_social_media';

  /**
   * The erecht_legal_texts supported languages.
   */
  const TEXTS_LANGUAGES = ['de'];

  /**
   * The erecht_legal_texts types.
   */
  const TEXTS_TYPES = [
    self::TYPE_IMPRINT,
    self::TYPE_PRIVACY_POLICY,
    self::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA,
  ];

  /**
   * The erecht_legal_texts to eRecht24 type mapping.
   */
  const PUSH_TYPE_MAPPING = [
    self::TYPE_IMPRINT => Helper::PUSH_TYPE_IMPRINT,
    self::TYPE_PRIVACY_POLICY => Helper::PUSH_TYPE_PRIVACY_POLICY,
    self::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA => Helper::PUSH_TYPE_PRIVACY_POLICY_SOCIAL_MEDIA,
  ];

}
