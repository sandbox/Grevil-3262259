<?php

namespace Drupal\erecht_legal_texts\Helper;

/**
 * Provides erecht_legal_texts validation functions.
 */
class Validation {

  /**
   * Provides eRecht API key validation.
   *
   * @param string $apiKey
   *   The eRecht api key.
   *
   * @return bool
   *   Whether or not the api key is valid.
   */
  public static function isApiKeyValid($apiKey) {
    if (empty($apiKey)) {
      return FALSE;
    }
    // @todo Get validation rules from erecht.
    if (strlen($apiKey) !== 64) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Provides eRecht plugin key validation.
   *
   * @param string $pluginKey
   *   The eRecht api key.
   *
   * @return bool
   *   Whether or not the plugin key is valid.
   */
  public static function isPluginKeyValid($pluginKey) {
    if (empty($pluginKey)) {
      return FALSE;
    }
    return TRUE;
  }

}
