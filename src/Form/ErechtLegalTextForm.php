<?php

namespace Drupal\erecht_legal_texts\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the erecht legal text entity edit forms.
 */
class ErechtLegalTextForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New erecht legal text %label has been created.', $message_arguments));
        $this->logger('erecht_legal_texts')->notice('Created new erecht legal text %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The erecht legal text %label has been updated.', $message_arguments));
        $this->logger('erecht_legal_texts')->notice('Updated erecht legal text %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.erecht_legal_text.collection');

    return $result;
  }

}
