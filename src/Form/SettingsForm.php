<?php

namespace Drupal\erecht_legal_texts\Form;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\erecht_legal_texts\Helper\Constants;
use Drupal\erecht_legal_texts\LegalTextManagerInterface;
use Drupal\erecht_legal_texts\Helper\Validation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Erecht-Legal-Texts settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The legalTextManager object.
   *
   * @var Drupal\erecht_legal_texts\LegalTextManagerInterface
   */
  protected $legalTextManager;

  /**
   * An eRechtTextObserverObject.
   *
   * @var Drupal\erecht_legal_texts\ErechtTextObserver
   */
  protected $eRechtTextObserver;

  /**
   * A date formatter object.
   *
   * @var Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Class constructor.
   */
  public function __construct(LegalTextManagerInterface $legalTextManager, DateFormatter $dateFormatter) {
    $this->legalTextManager = $legalTextManager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('erecht_legal_texts.legal_text_manager'),
      $container->get('date.formatter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'erecht_legal_texts_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['erecht_legal_texts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('erecht_legal_texts.settings');

    // Legal text entitites:
    if (!empty($this->config('erecht_legal_texts.settings')->get('api_key')) && !empty($this->config('erecht_legal_texts.settings')->get('plugin_key'))) {
      // We can not inject this,
      // because on construction of this form, the api-key is unknown:
      $this->eRechtTextObserver = \Drupal::service('erecht_legal_texts.text_observer');
      $imprintEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_IMPRINT);
      $privacyPolicyEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY);
      $privacyPolicySocialMediaEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA);
    }

    $form['etracker_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings'),
    ];

    $form['key_form'] = [
      '#type' => 'details',
      '#title' => $this->t('API-Key'),
      '#group' => 'etracker_vertical_tabs',
    ];

    $form['key_form']['api_key_notice'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h5>Please provide your Api and Plugin Key, or you will not be able to use this modules functionality!</h5>'),
    ];

    $form['key_form']['api_key_textfield'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Please enter your eRecht API Key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('The key consists of numerals and letters.'),
    ];

    $form['key_form']['plugin_key_textfield'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Please enter your eRecht Plugin Key'),
      '#default_value' => $config->get('plugin_key'),
      '#description' => $this->t('eRecht forces API consumers to also provide a developer key to identify the client. Contact eRecht to receive an own developer key, as the project is open source.<br/>In the eyes of the module maintainers, this concept should be removed by eRecht and instead only the API Key should be required to identify a client.'),
    ];

    // If the api or plugin key are not set, don't show the rest of form.
    if (empty($this->config('erecht_legal_texts.settings')->get('api_key')) || empty($this->config('erecht_legal_texts.settings')->get('plugin_key'))) {
      return parent::buildForm($form, $form_state);
    }

    $form['imprint_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Imprint'),
      '#description' => $this->t('This page provides several imprint related settings.'),
      '#group' => 'etracker_vertical_tabs',
    ];
    foreach (Constants::TEXTS_LANGUAGES as $lang_code) {
      $form['imprint_form']['imprint_enabled_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Enabled',
        '#default_value' => !empty($imprintEntity->get('enabled')->value),
        '#description' => $this->t('Check this checkbox to enable the "@lang" imprint.', ['@lang' => $lang_code]),
      ];
      $form['imprint_form']['imprint_custom_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Custom',
        '#default_value' => !empty($imprintEntity->get('custom')->value),
        '#description' => $this->t('Check this checkbox to enable modifying the legal text, Note, that this will disable automatic updates for this legal text.'),
        '#states' => [
          'visible' => [
            ':input[name="imprint_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['imprint_form']['imprint_auto_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Auto Update',
        '#default_value' => !empty($imprintEntity->get('auto_update')->value),
        '#description' => $this->t('Check this checkbox to enable automatic updates for the "@lang" imprint through cron.', ['@lang' => $lang_code]),
        '#states' => [
          'visible' => [
            ':input[name="imprint_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['imprint_form']['imprint_textarea_' . $lang_code] = [
        '#type' => 'text_format',
        '#title' => $this->t('Imprint HTML @lang', ['@lang' => $lang_code]),
        '#default_value' => $this->legalTextManager->getImprint($lang_code)['value'],
        '#format' => $this->legalTextManager->getImprint($lang_code)['format'],
        '#states' => [
          'invisible' => [
            // @todo This should be disabled, but it is currently not possible, because of this core issue: https://www.drupal.org/project/drupal/issues/3029417
            ':input[name="imprint_enabled_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
          'visible' => [
            ':input[name="imprint_custom_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['imprint_form']['imprint_updated_' . $lang_code] = [
        '#type' => 'item',
        '#title' => $this->t('Imprint HTML @lang last updated:', ['@lang' => $lang_code]),
        // @todo when we implement more languages, we need to get when the specific translation was updated last:
        '#markup' => $this->t('@imprintLastUpdated', ['@imprintLastUpdated' => $this->dateFormatter->format($imprintEntity->get('changed')->getString(), 'medium')]),
        '#states' => [
          'visible' => [
            ':input[name="imprint_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="imprint_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['imprint_form']['imprint_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Update Imprint ' . $lang_code,
        '#description' => $this->t('Fetches the newest "@lang" Imprint version', ['@lang' => $lang_code]),
        '#default_value' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="imprint_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="imprint_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
            ':input[name="imprint_auto_update_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
        ],
      ];
    }

    $form['privacy_policy_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Privacy Policy'),
      '#description' => $this->t('This page provides several privacy policy related settings.'),
      '#group' => 'etracker_vertical_tabs',
    ];

    foreach (Constants::TEXTS_LANGUAGES as $lang_code) {
      $form['privacy_policy_form']['privacy_policy_enabled_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Enabled',
        '#default_value' => !empty($privacyPolicyEntity->get('enabled')->value),
        '#description' => $this->t('Check this checkbox to enable the "@lang" privacy policy.', ['@lang' => $lang_code]),
      ];
      $form['privacy_policy_form']['privacy_policy_custom_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Custom',
        '#default_value' => !empty($privacyPolicyEntity->get('custom')->value),
        '#description' => $this->t('Check this checkbox to enable modifying the legal text, Note, that you can NOT enable this AND auto update.'),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_form']['privacy_policy_auto_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Auto Update',
        '#default_value' => !empty($privacyPolicyEntity->get('auto_update')->value),
        '#description' => $this->t('Check this checkbox to enable automatic updates for the "@lang" privacy policy through cron.', ['@lang' => $lang_code]),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_form']['privacy_policy_textarea_' . $lang_code] = [
        '#type' => 'text_format',
        '#title' => $this->t('Privacy policy HTML @lang', ['@lang' => $lang_code]),
        '#default_value' => $this->legalTextManager->getPrivacyPolicy($lang_code)['value'],
        '#format' => $this->legalTextManager->getPrivacyPolicy($lang_code)['format'],
        '#states' => [
          'invisible' => [
            // @todo This should be disabled, but it is currently not possible, because of this core issue: https://www.drupal.org/project/drupal/issues/3029417
            ':input[name="privacy_policy_enabled_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
          'visible' => [
            ':input[name="privacy_policy_custom_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_form']['privacy_policy_updated_' . $lang_code] = [
        '#type' => 'item',
        '#title' => $this->t('Privacy Policy HTML @lang last updated:', ['@lang' => $lang_code]),
        // @todo when we implement more languages, we need to get when the specific translation was updated last:
        '#markup' => $this->t('@PrivacyPolicytLastUpdated', ['@PrivacyPolicytLastUpdated' => $this->dateFormatter->format($privacyPolicyEntity->get('changed')->getString(), 'medium')]),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="privacy_policy_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_form']['privacy_policy_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Update Privacy Policy ' . $lang_code,
        '#description' => $this->t('Fetches the newest "@lang" privacy policy version', ['@lang' => $lang_code]),
        '#default_value' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="privacy_policy_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
            ':input[name="privacy_policy_auto_update_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
        ],
      ];
    }

    $form['privacy_policy_social_media_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Privacy Policy Social Media'),
      '#description' => $this->t('This page provides several privacy policy social media related settings.'),
      '#group' => 'etracker_vertical_tabs',
    ];

    foreach (Constants::TEXTS_LANGUAGES as $lang_code) {
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_enabled_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Enabled',
        '#default_value' => !empty($privacyPolicySocialMediaEntity->get('enabled')->value),
        '#description' => $this->t('Check this checkbox to enable the "@lang" privacy policy social media.', ['@lang' => $lang_code]),
      ];
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_custom_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Custom',
        '#default_value' => !empty($privacyPolicySocialMediaEntity->get('custom')->value),
        '#description' => $this->t('Check this checkbox to enable modifying the legal text, Note, that you can NOT enable this AND auto update.'),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_social_media_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_auto_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Auto Update',
        '#default_value' => !empty($privacyPolicySocialMediaEntity->get('auto_update')->value),
        '#description' => $this->t('Check this checkbox to enable automatic updates for the "@lang" privacy policy social media through cron.', ['@lang' => $lang_code]),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_social_media_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_textarea_' . $lang_code] = [
        '#type' => 'text_format',
        '#title' => $this->t('Privacy policy social media HTML @lang', ['@lang' => $lang_code]),
        '#default_value' => $this->legalTextManager->getPrivacyPolicySocialMedia($lang_code)['value'],
        '#format' => $this->legalTextManager->getPrivacyPolicySocialMedia($lang_code)['format'],
        '#states' => [
          'invisible' => [
            // @todo This should be disabled, but it is currently not possible, because of this core issue: https://www.drupal.org/project/drupal/issues/3029417
            ':input[name="privacy_policy_social_media_enabled_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
          'visible' => [
            ':input[name="privacy_policy_social_media_custom_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_updated_' . $lang_code] = [
        '#type' => 'item',
        '#title' => $this->t('Privacy policy social media HTML @lang last updated:', ['@lang' => $lang_code]),
        // @todo when we implement more languages, we need to get when the specific translation was updated last:
        '#markup' => $this->t('@PrivacyPolicySocialMediatLastUpdated', ['@PrivacyPolicySocialMediatLastUpdated' => $this->dateFormatter->format($privacyPolicySocialMediaEntity->get('changed')->getString(), 'medium')]),
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_social_media_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="privacy_policy_social_media_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['privacy_policy_social_media_form']['privacy_policy_social_media_update_checkbox_' . $lang_code] = [
        '#type' => 'checkbox',
        '#title' => 'Update Privacy Policy Social Media ' . $lang_code,
        '#description' => $this->t('Fetches the newest "@lang" Update privacy policy social media version', ['@lang' => $lang_code]),
        '#default_value' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="privacy_policy_social_media_custom_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
            ':input[name="privacy_policy_social_media_enabled_checkbox_' . $lang_code . '"]' => ['checked' => TRUE],
            ':input[name="privacy_policy_social_media_auto_update_checkbox_' . $lang_code . '"]' => ['checked' => FALSE],
          ],
        ],
      ];
    }

    // @todo Implement:
    // $form['module_status_form'] = [
    //   '#type' => 'details',
    //   '#title' => $this->t('Module Status'),
    //   '#description' => $this->t('On this page.'),
    //   '#group' => 'etracker_vertical_tabs',
    // ];

    $form['extra_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Extra'),
      '#description' => $this->t('Extra eRecht module functionalities'),
      '#group' => 'etracker_vertical_tabs',
    ];

    $form['extra_form']['update_all_checkbox'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Update All Legal Texts'),
      '#description' => $this->t('This option will ignore custom legal texts'),
    ];

    $form['extra_form']['enable_all_auto_update'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('all_auto_update'),
      '#title' => $this->t('Enables auto-update for all legal texts'),
      '#description' => $this->t('This option will ignore custom legal texts'),
    ];

    $form['extra_form']['register_observer'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Reregister the text observer for updating'),
      '#description' => $this->t('Checking this checkbox, will reregister the Observer for legal texts.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!Validation::isApiKeyValid($form_state->getValue('api_key_textfield'))) {
      $form_state->setErrorByName('api_key_textfield', $this->t('The api key is not valid!'));
    }
    if (!Validation::isPluginKeyValid($form_state->getValue('plugin_key_textfield'))) {
      $form_state->setErrorByName('plugin_key_textfield', $this->t('The plugin key is not valid!'));
    }
    foreach (Constants::TEXTS_LANGUAGES as $lang_code) {
      if (!empty($form_state->getValue('imprint_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('imprint_auto_update_checkbox_' . $lang_code))) {
        $form_state->setErrorByName('imprint_custom_checkbox_' . $lang_code, $this->t('It is not possible to Enable both "Custom" and "Auto Update" in "Imprint"'));
        $form_state->setErrorByName('imprint_auto_update_checkbox_' . $lang_code);
      }
      if (!empty($form_state->getValue('privacy_policy_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('privacy_policy_auto_update_checkbox_' . $lang_code))) {
        $form_state->setErrorByName('privacy_policy_custom_checkbox_' . $lang_code, $this->t('It is not possible to Enable both "Custom" and "Auto Update" in "Privacy Policy"'));
        $form_state->setErrorByName('privacy_policy_auto_update_checkbox_' . $lang_code);
      }
      if (!empty($form_state->getValue('privacy_policy_social_media_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('privacy_policy_social_media_auto_update_checkbox_' . $lang_code))) {
        $form_state->setErrorByName('privacy_policy_social_media_custom_checkbox_' . $lang_code, $this->t('It is not possible to Enable both "Custom" and "Auto Update" in "Privacy Policy Social Media"'));
        $form_state->setErrorByName('privacy_policy_social_media_auto_update_checkbox_' . $lang_code);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('erecht_legal_texts.settings');
    $config
      ->set('api_key', $form_state->getValue('api_key_textfield'))
      ->set('plugin_key', $form_state->getValue('plugin_key_textfield'))
      ->save();

    if ($config->get('api_key') != $form_state->getValue('api_key_textfield') || $config->get('plugin_key') != $form_state->getValue('plugin_key_textfield')) {
      $this->eRechtTextObserver->register();
    }

    if (!empty($config->get('api_key')) || !empty($config->get('plugin_key'))) {
      foreach (Constants::TEXTS_LANGUAGES as $lang_code) {
        $imprintEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_IMPRINT, $lang_code);
        $privacyPolicyEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY, $lang_code);
        $privacyPolicySocialMediaEntity = $this->legalTextManager->getLegalTextEntityByType(Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA, $lang_code);

        $imprintEntity->set('enabled', $form_state->getValue('imprint_enabled_checkbox_' . $lang_code));
        if (!empty($form_state->getValue('imprint_enabled_checkbox_' . $lang_code))) {
          $imprintEntity->set('custom', $form_state->getValue('imprint_custom_checkbox_' . $lang_code));
          $imprintEntity->set('auto_update', !empty($form_state->getValue('imprint_auto_update_checkbox_' . $lang_code)));
          $imprintEntity->set('legal_text', [
            'value' => $form_state->getValue('imprint_textarea_' . $lang_code)['value'],
            'format' => $form_state->getValue('imprint_textarea_' . $lang_code)['format'],
          ]);
        }
        $privacyPolicyEntity->set('enabled', $form_state->getValue('privacy_policy_enabled_checkbox_' . $lang_code));
        if (!empty($form_state->getValue('privacy_policy_enabled_checkbox_' . $lang_code))) {
          $privacyPolicyEntity->set('custom', $form_state->getValue('privacy_policy_custom_checkbox_' . $lang_code));
          $privacyPolicyEntity->set('auto_update', !empty($form_state->getValue('privacy_policy_auto_update_checkbox_' . $lang_code)));
          $privacyPolicyEntity->set('legal_text', [
            'value' => $form_state->getValue('privacy_policy_textarea_' . $lang_code)['value'],
            'format' => $form_state->getValue('privacy_policy_textarea_' . $lang_code)['format'],
          ]);
        }
        $privacyPolicySocialMediaEntity->set('enabled', $form_state->getValue('privacy_policy_social_media_enabled_checkbox_' . $lang_code));
        if (!empty($form_state->getValue('privacy_policy_social_media_enabled_checkbox_' . $lang_code))) {
          $privacyPolicySocialMediaEntity->set('custom', $form_state->getValue('privacy_policy_social_media_custom_checkbox_' . $lang_code));
          $privacyPolicySocialMediaEntity->set('auto_update', !empty($form_state->getValue('privacy_policy_social_media_auto_update_checkbox_' . $lang_code)));
          $privacyPolicySocialMediaEntity->set('legal_text', [
            'value' => $form_state->getValue('privacy_policy_social_media_textarea_' . $lang_code)['value'],
            'format' => $form_state->getValue('privacy_policy_social_media_textarea_' . $lang_code)['format'],
          ]);
        }
        if (!empty($form_state->getValue('imprint_update_checkbox_' . $lang_code))) {
          $this->legalTextManager->refreshLegalText(Constants::TYPE_IMPRINT, $lang_code);
        }
        if (!empty($form_state->getValue('privacy_policy_update_checkbox_' . $lang_code))) {
          $this->legalTextManager->refreshLegalText(Constants::TYPE_PRIVACY_POLICY, $lang_code);
        }
        if (!empty($form_state->getValue('privacy_policy_social_media_update_checkbox_' . $lang_code))) {
          $this->legalTextManager->refreshLegalText(Constants::TYPE_PRIVACY_POLICY_SOCIAL_MEDIA, $lang_code);
        }

        if (!empty($form_state->getValue('enable_all_auto_update'))) {
          if (empty($form_state->getValue('imprint_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('imprint_enabled_checkbox_' . $lang_code))) {
            $imprintEntity->set('auto_update', TRUE)->save();
          }
          if (empty($form_state->getValue('privacy_policy_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('privacy_policy_enabled_checkbox_' . $lang_code))) {
            $privacyPolicyEntity->set('auto_update', TRUE)->save();
          }
          if (empty($form_state->getValue('privacy_policy_social_media_custom_checkbox_' . $lang_code)) && !empty($form_state->getValue('privacy_policy_social_media_enabled_checkbox_' . $lang_code))) {
            $privacyPolicySocialMediaEntity->set('auto_update', TRUE)->save();
          }
        }
      }
      $imprintEntity->save();
      $privacyPolicyEntity->save();
      $privacyPolicySocialMediaEntity->save();

      if (!empty($form_state->getValue('update_all_checkbox'))) {
        $this->legalTextManager->refreshAllLegalTexts();
      }
      if (!empty($form_state->getValue('register_observer'))) {
        $this->eRechtTextObserver->updateRegistration();
      }
    }
    parent::submitForm($form, $form_state);
  }

}
